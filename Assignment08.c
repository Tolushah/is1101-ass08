/*Index no :- 19020325
Harindi.M.A.T.
Assignment 8 */

//Program to Store and display details of students
#include <stdio.h>

//Structure
struct student{
	char first_name[20];
	char subject[20];
	int marks;
	
} ;

int main(){
	    
	 	int n,m;
	 	//reading number of students
	    printf(" Enter the number of students (minimum 5) : ");
	    scanf("%d", &n);
	    
	    struct student s1[n];
	    
	    //Fetching data using inputs
	    for(m=0; m<n; m++){
		
	    printf(" \n Enter your first name : ");
	    scanf(" %s", s1[m].first_name);
	    
	    printf(" Enter the subject : ");
	    scanf(" %s", s1[m].subject);
	    
	    printf(" Enter the marks : ");
	    scanf(" %d", &s1[m].marks);
	    printf("\n\n --------------------------------------------------------\n");
	}
	
		//Displaying student details
		printf(" \n\t\t Student Details\n");
		printf(" --------------------------------------------------------\n");
	    for(m=0; m<n; m++){
	    printf(" \n STUDENT %d\n",m+1);
	    printf(" \n\t First name : %s \n", s1[m].first_name);
	    printf(" \t Subject : %s\n", s1[m].subject);
	    printf(" \t Marks : %d\n", s1[m].marks);
	    printf(" ---------------------------------------------------------\n");
}
return 0;
}




